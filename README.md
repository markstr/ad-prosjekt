# Active Directory Mappeoppgave 2

Vi forestiller oss at ved et scenario hvor det er nødvendig å lage et helt system for grupper, OU-er, grupper, osv. så er det viktig å få systemet oppe å kjøre så fort som mulig igjen. Alt som kreves for disse scriptene som får alt opp igjen, er en domenekontroller med active directory på. Systemadmin kan så laste ned alle filene i denne i dette repositoriet i en mappe, og kjøre setup.ps1 for å få systemet opp igjen.  Vi antar at domenet har vært, og alltid forblir det samme, og at poenget med scriptene er å få et tidligere rotete system oppe å kjøre igjen på samme server. 

Active directory er en katalogtjeneste for håndtering av brukere, brukerrettigheter og ressurskontroll. Det er utviklet av Microsoft og er kjernen av alle Windows domenenettverk. Det lager informasjon om medlemmer av domenet, deres enheter og brukere samtidig som det verifiseres legitimasjonen definerer tingangsrettighetene deres. I denne readme.md'en skal vi gå igjennom hvordan vi valgte å løse Mappe Oppgave 2 i DCS(GT)1005 samt se på hvorfor disse løsningene er ideelle fra et sikkerhetsmessig ståpunkt.


## Beskrivelse og målet av prosjektet
Active Directory har egenskaper som tillater bedrifter å sette opp og sentralisere ulike tilganger i et nettverk. Dette bidrar til å styrke sikkerheten til bedriften ettersom man ønsker ikke at enhver bruker eller datamaskin skal ha tilgang til alle filer og/eller tjenester. Derfor er det viktig at strukturen er satt opp på en slik måte som gjenspeiler adgangshierarkiet.

![Domenemodell](https://i.ibb.co/SX6Lt2K/Blank-diagram-1.png)

Diagrammet her viser hvordan strukturen til bedriften er delt inn i ulike OU (Organizational Unit). På denne måten blir det mulig for en systemadministrator å delegere og kontrollere tilgangene til de ulike brukerne og gruppene. Dette gjøres ved at det dannes ulike konteinere innenfor domenet. Disse konteinerne muliggjør en mer presis autoritetsinndeling og tillater å sette policies for de gjeldene konteinerne. Tilgangene til hver enhet vil variere, og derfor er det essensielt at man har et system som gjør det både oversiktlig og enklere å ha kontroll. 

## Tilgangsinndeling

For at brukere og maskiner skal ha ulike egenskaper så brukes sikkerhetsgrupper. Dette er noe som grupperer sammen brukere og gir dem tilgang til samme spesifikke tjenester eller filer. Det er en kostnadseffektiv måte å tildele rettigheter på, og gjør det mulig å gi hele grupper tilgang på ressurser (f.eks. printere eller dører), istedenfor å måtte mikro-administrere alle brukere hver for seg. Man kan på lik linje bruke samme metode for å tildele rettigheter til datamaskiner. 

For å registrere en ny bruker, kjører sysadmin new_user.ps1. Den filen vil gi brukeren mulighet til å registrere en ny bruker ved å skrive inn fornavn, etternavn, departement/hvilken gruppe personen tilhører, og mailen. Scriptet vil så først sjekke om personen finnes fra før av. Dersom det ikke gjør det, så vil scriptet lage et entydig brukernavn til personen som tar hensyn til eksisterende brukernavn slik at det aldri blir to stykker med samme brukernavn.
I både setup.ps1 og new_users.ps1, så vil brukerne bli registrert med et tilfeldig laget passord som må byttes ved første innlogging. Passordet blir aldri laget, men sendt på mail til personen. Dette er for at en eventuell angriper aldri skal kunne logge seg inn hos noen dersom de ikke bytter passord med en gang en bruker blir registrert eller scriptene kjørt.

Når den nye brukeren lages, så defineres gruppen (OU) brukeren skal tilhøre. Det er denne gruppen som tildeler rettigheter og ressurstilgang. Eksempelvis så har man et globalt element som gir alle gruppene tilgang til alle dørene. Hvis det antas at bedriften holder til i et kontorbygg med flere andre bedrifter vil det derfor være fordelaktig å ha en global sikkerhetsgruppe. På den måten så kan gruppen få tilgang til ressurser i et annet domene. (Eksempelvis domenet «Kontorbygg» som har satt opp en 1-veistrust til bedriften). På den måten så kan alle de ulike domenene som opererer utfra kontorbygget få tilgang til dørene uten å måtte generere en ny bruker hos kontorbygg. 

På den andre siden så er det ikke alltid forsvarlig å gi alle gruppene samme tilgang. Dette kan løses ved å sette lokale domenegrupper. Her vil man kunne begrense tilgangene enhver gruppe har. Eksempelvis sperre eller kraftig begrense tilgangen renholds-gruppen har til bedriftens mapper. Lokale sikkerhetsgrupper er derfor essensielt for å kunne ha et nettverkssystem hvor ressurser og tjenester blir fordelt kun til de som trenger det. Dette er hvorfor vi deler domenet opp i OU og grupper, og er selve essensen i hvorfor Active Directory er et så viktig og nyttig verktøy.

## Group Policy

Ved å kun gi tilgang til de som trenger det, kan man unngå å lage unødige sikkerhetshull. Derfor brukes Group Policy for å kunne begrense tilgangen til ulike tjenester. Vi valgte derfor å sette opp et par policies som hjelper å sikre bedriftens maskiner/servere. Først og fremst så har vi en policy som sperrer tilgangen til kontrollpanelet. Dette er fordi kontrollpanelet gir tilgang til programmer og tjenester som kan både gjøre skade samt rote til datamaskinen(e). Kun kompetente personer bør ha tilgang til dette grensesnittet, og det er derfor kun tilgang blir gitt til IT administratorer. Vi har også satt en GPO som gjør det mulig å koble til datamaskinene sine gjennom Remote Desktop Connection. Dette er en policy som er spesielt nyttig i disse koronatider, ettersom at man kan jobbe gjennom fjernstyring. Like, -hvis ikke viktigere enn å beskytte datamaskinene sine, er det å beskytte de ansatte. Derfor ønskelig at de kan ha tilgang til datamaskinene selv om de er utenfor domenet. I tillegg, så er det rett og slett praktisk. 

Videre, så har vi en GPO som forhindrer at eksterne lagringsenheter (f.eks. USB Disk) kobler seg opp mot bedriftens stasjonære datamaskiner. Dette har hovedsakelig to hensikter. Først å fremst så er det en stor risiko for bedriften å ha datamaskiner tilgjengelig for alle brukerne. Dette er spesielt med tanke på at skadevare kan infisere datamaskinen og deretter utsette flere av brukerne og deretter bedriften for stor skade. En ekstern lagringsenhet kan i tillegg til å ødelegge data; -også stjele data. Tapt eller lekket åndsverk kan være fatalt for enhver bedrift, og er det er derfor viktig å ta enhver mulighet for å isolere en slik mulighet.



### Et mulig sikkerhetstiltak
Et sikkerhetstiltak som vi tenkte på er å logge MAC-adressene til maskinen som logger seg på IT-admins. Hvis et brukernavn og passord fra IT-admins skulle kommet på avveie, så er det lite som forhindrer den personen i å logge seg inn på IT-admins siden maskinene deres er på samme domenet.
Det finnes sikkert noen group policies som gjelder for maskinen som kan hindre dette. Hvis det ikke gjør det, kunne whitelisting av MAC-adresser vært en reell mulighet. På den måten kan systemadministrator bli varslet dersom noen andre enn IT-admins hadde kommet seg inn på brukerne deres.


## Kildeliste

Microsoft. (2017, 19 04). _Active Directory Security Groups._ Hentet Fra Microsoft: https://docs.microsoft.com/en-us/windows/security/identity-protection/access-control/active-directory-security-groups

Microsoft. (2017, 31 05). _Active Directory Domain Services_ Hentet fra Microsoft: https://docs.microsoft.com/en-us/windows-server/identity/ad-ds/active-directory-domain-services

Hjelmås, E. (Red.) (2021). _Notes.md_ Hentet Fra: https://gitlab.com/erikhje/dcsg1005/-/blob/master/group-policy/notes.md og https://gitlab.com/erikhje/dcsg1005/-/blob/master/logging-monitoring/notes.md

Melling, Hjelmås. NTNU (2021.) Videoforelesninger og Presentasjoner.

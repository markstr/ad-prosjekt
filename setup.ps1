Add-Type -AssemblyName 'System.Web'
# Deklarering av variabler for fleksibilitet blandt brukere:
$folderPath = "C:\Users\Administrator\Desktop\AD\ad-prosjekt"
$fullDomain = "sec.core"

$DCdomain = ""
for ($i = 0; $i -lt $fullDomain.split(".").length; $i++) {    
$part = $fullDomain.split(".")[$i]
$DCdomain += ",DC=$part"
}

# Lager OUene fra csv-fila
$AD_OU = Import-csv "$folderPath\OU.csv" -Delimiter "," 
foreach ($info in $AD_OU)
{   
    if($info.path){
    Write-Host $info.name " -Description " $info.description " -Path " $info.path
    New-ADOrganizationalUnit $info.name -Description $info.description -Path $info.path
    }
    else {
    Write-Host $info.name " -Description " $info.description
    New-ADOrganizationalUnit $info.name -Description $info.description
    }
}


Get-ADComputer "srv1" | Move-ADObject -TargetPath "OU=opit-servers,OU=opit-computers$DCdomain" -Verbose
Get-ADComputer "cli1" | Move-ADObject -TargetPath "OU=opit-workstations,OU=opit-computers$DCdomain" -Verbose


$AD_Groups = Import-csv "$folderPath\Groups.csv" -Delimiter ","
foreach ($info in $AD_Groups){
    Write-Host "-Name " $info.name " -Path " $info.path " -SamAccountName " $info.samaccountname
    New-ADGroup -GroupCategory Security -GroupScope Global -Name $info.name -Path $info.path -SamAccountName $info.samaccountname
}



$ressurser=@('printere','adgangskort')
$ressurser | ForEach-Object { New-ADGroup -Name $_ -Path "OU=ressurser$DCdomain" -GroupCategory Security -GroupScope DomainLocal }


$ADUsers = Import-csv "$folderPath\Users.csv" -Delimiter "," 
# username,firstname,lastname,upn,password,department,path
foreach ($person in $ADUsers)
{
	
function Get-RandomCharacters($length, $characters) { 
    $random = 1..$length | ForEach-Object { Get-Random -Maximum $characters.length } 
    $private:ofs="" 
    return [String]$characters[$random]
}

$passord = Get-RandomCharacters -length 10 -characters 'abcdefghijklmonpqrstuvwxyz123456789!"§$%&/()=?}][{@#*+ABCDEFGHIJKLMONPQRSTUVWXYZ'

$username = $person.username
$username = $username.Tolower().Replace('æ','ae').Replace('ø','o').Replace('å','aa')
$upn = $person.upn
$upn = $upn.Tolower().Replace('æ','ae').Replace('ø','o').Replace('å','aa')

    New-ADUser `
    -SamAccountName $username `
    -UserPrincipalName $upn `
    -Name "${person.firstname} ${person.lastname}" `
    -GivenName $firstname `
    -Surname $person.lastname `
    -Enabled $true `
    -EmailAddress $person.email `
    -ChangePasswordAtLogon $true `
    -DisplayName "${person.firstname} ${person.lastname}" `
    -Department $person.department `
    -Path $person.path `
    -AccountPassword (convertto-securestring $passord -AsPlainText -Force) 
    

    Write-Host $username $upn $person.firstname $person.lastname $person.department $person.password


$passwordEmail = "Adminadmin123456789" | convertTo-SecureString -AsPlainText -Force
$usernameEmail = "testjonathan1234@outlook.com" 
$credential = New-Object System.Management.Automation.PSCredential($usernameEmail, $passwordEmail)
Send-MailMessage -To $person.email -From $usernameEmail -Subject "Passord" -Body 'Ditt passord er: ' $passord -Credential $credential -SmtpServer "smtp-mail.outlook.com" -Port 587 -UseSsl
}

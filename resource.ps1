function get-Resource {

    param (
        $ComputerName
    )
    

    Enter-PSSession -ComputerName $ComputerName
    $resource = (GET-COUNTER -Counter "\Processor(_Total)\% Processor Time" -SampleInterval 1 -MaxSamples 4 |select -ExpandProperty countersamples | select -ExpandProperty cookedvalue | Measure-Object -Average).average 
    Write-Output "$ComputerName average usage $resource %"
    Exit-PSSession
    
    


}

Get-ADComputer -Filter 'operatingsystem -notlike "*server*" -and enabled -eq "true"' `
-Properties Name | Select-Object -ExpandProperty Name | ForEach-Object -Process {get-Resource($_)}

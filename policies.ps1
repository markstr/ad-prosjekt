$folderPath = "C:\Users\Administrator\Desktop\AD\ad-prosjekt"

# Forhindrer kontrollpanel for alle utenom IT-admins
# Lar alle koble seg til maskinene sine med Remote Desktop
New-GPO -Name "Prevent_control_panel" -Comment "Disables all Control Panel programs and the PC settings app."
New-GPO -Name "Allow_remote_desktop" -Comment "This policy allows you to configure remote access to computers by using Remote Desktop Services."

Set-GPRegistryValue -Name "Prevent_control_panel" -Key "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" -Type String -Value NoControlPanel
Set-GPRegistryValue -Name "Allow_remote_desktop" -Key "HKLM\SOFTWARE\Policies\Microsoft\Windows NT\Terminal Services" -Type String -ValueName fDenyTSConnections -Value 0

$AD_Groups = Import-csv "$folderPath\Groups.csv" -Delimiter ","
foreach ($info in $AD_Groups){
    New-GPLink -Name "Allow_remote_desktop" -Target $info.path

    if($info.name -ne "g_it_admins"){
    New-GPLink -Name "Prevent_control_panel" -Target $info.path
    }
}

# Fohindrer eksterne lagringsenheter på Stasjonære maskiner
New-GPO -Name "Prevent_external_storage_programs" -Comment "Configure access to all removable storage classes."
Set-GPRegistryValue -Name "Prevent_external_storage_programs" -Key "HKLM\Software\Policies\Microsoft\Windows\RemovableStorageDevices" -Type String -Value Deny_All
New-GPLink -Name "Prevent_external_storage_programs" -Target "OU=opit-workstations,OU=opit-computers,DC=sec,DC=core"

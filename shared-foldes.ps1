#script to create shared folders and give premmisions on that group

$domene = "sec.core"
$computer = "JKDDC1"
$location = "adprosjekt"


Get-WindowsFeature -Name fs*
Install-WindowsFeature -Name FS-DFS-Namespace,FS-DFS-Replication,RSAT-DFS-Mgmt-Con -IncludeManagementTools
Get-WindowsFeature -Name fs*
Import-Module dfsn



#format: file location, group, and premissions gropus should have
$files = @(("C:\shares\allersiste","geigei", "write, readandexecute"),("C:\shares\sllersitse44","geigei", "full"))
$files | foreach{
    mkdir -path $_[0]
    $sharename = (Get-Item $_[0]).name; New-SMBShare -Name $shareName -Path $_[0] -FullAccess administrators
    $newAcl = get-acl -path $_[0]
    $identity = $_[1]
    $fileSystemRights = $_[2]
    $type = "Allow"
    $fileSystemAccessRuleArgumentList = $identity, $fileSystemRights, $type
    $fileSystemAccessRule = New-Object -TypeName System.Security.AccessControl.FileSystemAccessRule -ArgumentList $fileSystemAccessRuleArgumentList
    $NewAcl.SetAccessRule($fileSystemAccessRule)
    Set-Acl -Path $_[0] -AclObject $NewAcl
}

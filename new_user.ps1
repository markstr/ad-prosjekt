$folderPath = "C:\Users\Administrator\Desktop\AD\ad-prosjekt"
$Users = Import-csv "$folderPath\Users.csv" -Delimiter ","

$fullDomain = "sec.core"

# Gjør domenet om til f.eks "DC=sec,DC=core"
$DCdomain = ""
for ($i = 0; $i -lt $fullDomain.split(".").length; $i++) {    
$part = $fullDomain.split(".")[$i]
$DCdomain += ",DC=$part"
}

$firstname = Read-host "Enter firstname"
$lastname = Read-host "Enter lastname"

# Sjekker om personen eksisterer opp mot fornavn og etternavn
$Users | ForEach-object {
    if ($firstname -eq $_.firstname -And $lastname -eq $_.lastname) {
        write-host "Person either exist or should change their name."
        exit
    }
}

function Get-RandomCharacters($length, $characters) { 
    $random = 1..$length | ForEach-Object { Get-Random -Maximum $characters.length } 
    $private:ofs="" 
    return [String]$characters[$random]
}

# All informasjonen som kreves for registrering av ny bruker 
$department = Read-host "Which department does the person belong to"
$email = Read-host "Enter email"
$upn = "$firstname.$lastname@$fullDomain"
$path = "OU=" + $department + ",OU=opit-users" + $DCdomain
$password = Get-RandomCharacters -length 10 -characters 'abcdefghijklmonpqrstuvwxyz123456789!"§$%&/()=?}][{@#*+ABCDEFGHIJKLMONPQRSTUVWXYZ'

# username,firstname,lastname,upn,department,path,email

function newUser {
    Write-Host "$username, $firstname, $lastname, $upn, $department, $path, $email"
    "$username,$firstname,$lastname,$upn,$department,`"$path`",$email" >> "$folderPath\Users.csv"

    New-ADUser `
    -SamAccountName $username `
    -UserPrincipalName $upn `
    -Name "$firstname $lastname" `
    -GivenName $firstname `
    -Surname $lastname `
    -Enabled $true `
    -EmailAddress $email `
    -ChangePasswordAtLogon $true `
    -DisplayName "$firstname $lastname" `
    -Department $department `
    -Path $path `
    -AccountPassword (convertto-securestring $password -AsPlainText -Force)

    $passwordEmail = "Adminadmin123456789" | convertTo-SecureString -AsPlainText -Force
    $usernameEmail = "testjonathan1234@outlook.com" 
    $credential = New-Object System.Management.Automation.PSCredential($usernameEmail, $passwordEmail)
    Send-MailMessage -To $person.email -From $usernameEmail -Subject "Passord" -Body 'Ditt passord er: ' $passord -Credential $credential -SmtpServer "smtp-mail.outlook.com" -Port 587 -UseSsl

}

$username = ""
$iFirstname = 3;
$iLastname = 3;
$s_firstname = $firstname.toLower().Replace('æ','ae').Replace('ø','o').Replace('å','aa')
$s_lastname = $lastname.toLower().Replace('æ','ae').Replace('ø','o').Replace('å','aa')

# Sjekker om det allerede finnes et brukernavn.
# Hvis det gjør det, får det nye brukernavnet en ekstra bokstav annenhver fra fornavnet og etternavnet
function createUsername {

    $username = $s_firstname.Substring(0, $iFirstname) + $s_lastname.Substring(0, $iLastname)
    Write-Host "Username er '$username'"

    $error.clear()
    try {Get-ADUser -Identity $username}
    catch {newUser}

    if (!$error) {
        if (($iFirstname - $iLastname) -gt 0) {
            $iLastname++
            createUsername
        }
        else {
            $iFirstname++
            createUsername
        }
    }
}

createUsername
